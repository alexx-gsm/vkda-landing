import { compose, withProps } from 'recompose'
// styles
import { withStyles } from '@material-ui/styles'
import styles from './styles'
// component
import Slider from './Slider'
// config
import config from './config'

export default compose(
  withProps({ config, images: config.images }),
  withStyles(styles)
)(Slider)

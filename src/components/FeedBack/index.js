import { compose } from 'recompose'
// styles
import { withStyles } from '@material-ui/styles'
import styles from './styles'
import CommonStyles from '../../styles/CommonStyles'
// component
import FeedBack from './FeedBack'

export default compose(
  withStyles((theme) => ({ ...CommonStyles(theme), ...styles(theme) }), {
    withTheme: true,
  })
)(FeedBack)

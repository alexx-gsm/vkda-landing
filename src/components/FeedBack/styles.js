export default (theme) => ({
  Title: {
    padding: `${theme.spacing(4)}px 0`,
    fontFamily: '"Philosopher", sans-serif',
    fontWeight: theme.typography.fontWeightRegular,
    [theme.breakpoints.down('xs')]: {
      fontSize: '2.2rem',
    },
  },
  GroupRoot: {
    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
    },
  },
})

import React, { useState } from 'react'
import { Container, Box, Typography, Hidden } from '@material-ui/core'
import { TextField, Button } from '@material-ui/core'
import { RadioGroup, FormControlLabel, Radio } from '@material-ui/core'
import { ToggleButtonGroup, ToggleButton } from '@material-ui/lab'

function FeedBack({ classes }) {
  const [values, setValues] = useState({
    type: '1',
    name: '',
    phone: '',
    comment: '',
  })

  const handleChange = (event, newType) => {
    setValues({ ...values, type: newType })
  }

  const handleInput = (name) => (event) => {
    setValues({ ...values, [name]: event.target.value })
  }

  return (
    <Container maxWidth="md" className={classes._MB8}>
      <Typography
        variant="h2"
        className={classes.Title}
        align="center"
        color="primary"
      >
        Обратная связь
      </Typography>

      <Hidden smUp>
        <Container maxWidth="xs">
          <RadioGroup
            name="type"
            className={classes.group}
            value={values.type}
            onChange={handleInput('type')}
          >
            <FormControlLabel
              value="1"
              control={<Radio />}
              label="Предложение"
            />
            <FormControlLabel
              value="2"
              control={<Radio />}
              label="Благодарность"
            />
            <FormControlLabel value="3" control={<Radio />} label="Жалоба" />
          </RadioGroup>
        </Container>
      </Hidden>

      <Box align="center">
        <Hidden xsDown>
          <ToggleButtonGroup
            size="small"
            value={values.type}
            exclusive
            onChange={handleChange}
            classes={{ root: classes.GroupRoot }}
          >
            <ToggleButton value="1">
              <Typography variant="h6">Предложение</Typography>
            </ToggleButton>
            <ToggleButton value="2">
              <Typography variant="h6">Благодарность</Typography>
            </ToggleButton>
            <ToggleButton value="3">
              <Typography variant="h6">Жалоба</Typography>
            </ToggleButton>
          </ToggleButtonGroup>
        </Hidden>
      </Box>

      <Container maxWidth="sm">
        <TextField
          id="name"
          label="Имя"
          className={classes.textField}
          value={values.name}
          onChange={handleInput('name')}
          margin="normal"
          fullWidth
        />
        <TextField
          id="phone"
          label="Телефон"
          className={classes.textField}
          value={values.phone}
          onChange={handleInput('phone')}
          margin="normal"
          fullWidth
        />
        <TextField
          id="comment"
          label="Комментарий"
          className={classes.textField}
          value={values.comment}
          onChange={handleInput('comment')}
          margin="normal"
          multiline
          rowsMax="4"
          rows="2"
          fullWidth
        />
        <Box align="center" className={classes._PT5}>
          <Button variant="contained" color="secondary" size="medium">
            Отправить
          </Button>
        </Box>
      </Container>
    </Container>
  )
}

export default FeedBack

import { compose } from 'recompose'
// styles
import { withStyles } from '@material-ui/styles'
import styles from './styles'
import CommonStyles from '../../styles/CommonStyles'
// component
import Footer from './Footer'

export default compose(
  withStyles((theme) => ({ ...CommonStyles(theme), ...styles(theme) }), {
    withTheme: true,
  })
)(Footer)

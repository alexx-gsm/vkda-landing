import { compose, withProps } from 'recompose'
// styles
import { withStyles } from '@material-ui/styles'
import styles from './styles'
// component
import ComplexDishDialog from './ComplexDishDialog'

export default compose(
  withProps({
    formatter: new Intl.NumberFormat('ru-ru', {
      style: 'currency',
      currency: 'RUB',
      minimumFractionDigits: 0,
    }),
  }),
  withStyles(styles)
)(ComplexDishDialog)

import React from 'react'
import { Button, Box, Grid, Typography } from '@material-ui/core'
import { Dialog } from '@material-ui/core'
import { DialogTitle, DialogContent, DialogActions } from '@material-ui/core'

function ComplexDishDialog({
  complexDinner,
  open,
  handleClose,
  formatter,
  classes,
}) {
  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="form-dialog-title"
      fullWidth
      maxWidth={'xs'}
      classes={{
        paper: classes.DialogPaper,
      }}
    >
      <DialogTitle id="form-dialog-title" disableTypography>
        <Typography variant="h4" align="center" color="primary">
          Обед "{complexDinner.title}"
        </Typography>
      </DialogTitle>
      <DialogContent classes={{ root: classes.DialogContentRoot }}>
        {complexDinner.dishes.map((dish) => (
          <Grid container wrap="nowrap" spacing={2} key={dish._id}>
            <Grid item>
              <Box className={classes.ImageWrap}>
                <img
                  src={`assets/images/dishes/${dish.image}`}
                  alt={dish.title}
                />
              </Box>
            </Grid>
            <Grid item container direction="column">
              <Grid item>
                <Typography variant="h6" className={classes.DishTitle}>
                  {dish.title}
                </Typography>
              </Grid>
              <Grid item>
                <Typography variant="caption" className={classes.DishContent}>
                  {dish.content}
                </Typography>
              </Grid>
              <Grid item>
                <Typography variant="caption" className={classes.DishWeight}>
                  {dish.weight}г.
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        ))}
      </DialogContent>
      <DialogActions>
        <Typography variant="h4" className={classes.Price}>
          {formatter.format(complexDinner.price)}
        </Typography>
        <Button onClick={handleClose} color="primary">
          Отмена
        </Button>
        <Button onClick={handleClose} color="secondary" variant="contained">
          Заказать
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default ComplexDishDialog

export default (theme) => ({
  DialogPaper: {
    [theme.breakpoints.down('sm')]: {
      margin: theme.spacing(2),
    },
  },
  DialogContentRoot: {
    padding: theme.spacing(1),
  },
  ImageWrap: {
    width: '80px',
    height: '80px',
    overflow: 'hidden',
    display: 'flex',
    justifyContent: 'center',
    '& img': {
      height: '100%',
    },
  },
  DishTitle: {
    color: theme.palette.secondary.dark,
    lineHeight: '1.2',
    marginBottom: theme.spacing(1),
    textDecoration: 'underline',
  },
  DishContent: {
    color: theme.palette.grey[800],
    display: 'block',
    lineHeight: '1.2',
  },
  DishWeight: {
    fontWeight: theme.typography.fontWeightMedium,
    fontSize: '1rem',
  },
  Price: {
    color: theme.palette.secondary.main,
    marginRight: 'auto',
  },
})

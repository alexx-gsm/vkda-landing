// styles
import { withStyles } from '@material-ui/styles'
import styles from './styles'
// component
import TopPanel from './TopPanel'

export default withStyles(styles)(TopPanel)

import { compose, withProps } from 'recompose'
// styles
import { withStyles } from '@material-ui/styles'
import styles from './styles'
import CommonStyles from '../../styles/CommonStyles'
// component
import FavoriteDish from './FavoriteDish'

export default compose(
  withProps({
    favoriteDish: {
      title: 'Мимоза',
      category: 'Салат',
      content: 'Картоха, морковь, сайра в масле, яйцо вареное, майонез',
      price: '50',
    },
    formatter: new Intl.NumberFormat('ru-ru', {
      style: 'currency',
      currency: 'RUB',
      minimumFractionDigits: 0,
    }),
  }),
  withStyles((theme) => ({ ...CommonStyles(theme), ...styles(theme) }), {
    withTheme: true,
  })
)(FavoriteDish)

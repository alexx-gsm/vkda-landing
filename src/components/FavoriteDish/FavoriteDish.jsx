import React from 'react'
import { Container, Box, Grid, Typography } from '@material-ui/core'
import { Button } from '@material-ui/core'

function FavoriteDish({ favoriteDish, formatter, classes }) {
  return (
    <Box className={classes.ImageBox}>
      <Box className={classes.ImageWrap}>
        <img
          src={`assets/images/dishes/favorite.jpg`}
          alt="Блюдо дня"
          className={classes.Image}
        />

        <Box className={classes.ImageGradient} />
      </Box>

      <Box className={classes.InfoWrap}>
        <Container maxWidth="lg" classes={{ root: classes.InfoContainerRoot }}>
          <Grid
            container
            direction="column"
            wrap="nowrap"
            className={classes.GridInfo}
          >
            <Grid item>
              <Typography variant="h2" className={classes.Title} align="center">
                Блюдо дня
              </Typography>
            </Grid>
            <Grid item className={classes.GridInfoTitle}>
              <Typography variant="h2" className={classes.InfoTitle}>
                {favoriteDish.title}
              </Typography>
            </Grid>
            <Grid item>
              <Typography variant="h6" className={classes.InfoSubTitle}>
                {favoriteDish.content}
              </Typography>
            </Grid>
            <Grid item>
              <Typography variant="h2" className={classes.InfoPrice}>
                {formatter.format(favoriteDish.price)}
              </Typography>
            </Grid>
            <Grid item className={classes.GridActions}>
              <Button variant="contained" color="secondary" size="large">
                В корзину
              </Button>
            </Grid>
          </Grid>
        </Container>
      </Box>
    </Box>
  )
}

export default FavoriteDish

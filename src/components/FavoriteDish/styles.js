export default (theme) => ({
  ImageBox: {
    paddingTop: '30%',
    position: 'relative',
    overflow: 'hidden',
    height: 0,
    [theme.breakpoints.down('lg')]: {
      paddingTop: '30%',
    },
    [theme.breakpoints.down('md')]: {
      paddingTop: 0,
      height: '50vh',
    },
    [theme.breakpoints.down('xs')]: {
      paddingTop: 0,
      height: '100vh',
    },
  },
  ImageWrap: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '100%',
    [theme.breakpoints.down('xs')]: {
      paddingTop: 0,
      height: '100vh',
    },
  },
  Image: {
    maxWidth: '100%',
    [theme.breakpoints.down('sm')]: {
      maxWidth: 'initial',
      height: '100vh',
    },
  },
  ImageGradient: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    background: 'rgba(0, 0, 0, 0.5)',
  },
  InfoWrap: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
  },
  InfoContainerRoot: {
    width: '100%',
    height: '100%',
    [theme.breakpoints.down('xs')]: {
      paddingTop: theme.spacing(7),
    },
  },
  GridInfo: {
    width: '100%',
    height: '100%',
    [theme.breakpoints.down('xs')]: {
      alignItems: 'center',
    },
  },
  Title: {
    padding: `${theme.spacing(4)}px 0`,
    fontFamily: '"Philosopher", sans-serif',
    fontWeight: theme.typography.fontWeightRegular,
    color: theme.palette.common.white,
    [theme.breakpoints.down('xs')]: {
      fontSize: '2.2rem',
    },
  },
  GridInfoTitle: {
    [theme.breakpoints.down('xs')]: {
      marginTop: 'auto',
    },
  },
  InfoTitle: {
    fontWeight: theme.typography.fontWeightMedium,
    color: theme.palette.common.white,
    [theme.breakpoints.down('sm')]: {
      fontSize: '3rem',
    },
  },
  InfoSubTitle: {
    color: theme.palette.grey[300],
    paddingBottom: theme.spacing(2),
    fontWeight: theme.typography.fontWeightLight,
    [theme.breakpoints.down('xs')]: {
      fontSize: '1.2rem',
      lineHeight: 1.2,
      textAlign: 'center',
    },
  },
  InfoPrice: {
    color: theme.palette.common.white,
    fontWeight: theme.typography.fontWeightLight,
  },
  GridActions: {
    marginTop: theme.spacing(7),
    [theme.breakpoints.down('xs')]: {
      alignSelf: 'center',
      marginTop: 'auto',
      marginBottom: theme.spacing(7),
    },
  },
})

import { compose } from 'recompose'
// styles
import { withStyles } from '@material-ui/styles'
import styles from './styles'
// component
import Discounts from './Discounts'

export default compose(withStyles(styles))(Discounts)

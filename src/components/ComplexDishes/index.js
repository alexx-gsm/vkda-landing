import { compose, withProps } from 'recompose'
// mock data
import { dishes, complexDinners } from '../../mock/dishes.js'
// styles
import { withStyles } from '@material-ui/styles'
import styles from './styles'
// component
import ComplexDishes from './ComplexDishes'

export default compose(
  withProps({
    dishes,
    complexDinners,
    formatter: new Intl.NumberFormat('ru-ru', {
      style: 'currency',
      currency: 'RUB',
      minimumFractionDigits: 0,
    }),
  }),
  withStyles(styles)
)(ComplexDishes)

export default (theme) => ({
  Container: {
    paddingBottom: theme.spacing(10),
    [theme.breakpoints.down('xs')]: {
      paddingLeft: 0,
      paddingRight: 0,
      paddingBottom: theme.spacing(2),
    },
  },
  Title: {
    padding: `${theme.spacing(4)}px 0`,
    fontFamily: '"Philosopher", sans-serif',
    fontWeight: theme.typography.fontWeightRegular,
    [theme.breakpoints.down('xs')]: {
      fontSize: '2.2rem',
    },
  },
  TabsWrap: {
    marginBottom: theme.spacing(3),
  },
  GridCard: {
    [theme.breakpoints.down('sm')]: {
      maxWidth: '480px',
      marginLeft: 'auto',
      marginRight: 'auto',
      '&:not(:last-child)': {
        paddingBottom: 0,
      },
    },
  },
  CardWrap: {
    border: '1px solid rgba(244, 67, 54, 0.2)',
  },
  CardTitle: {
    paddingTop: theme.spacing(1),
  },
  CardSubTitle: {
    fontSize: '1rem',
    color: theme.palette.grey[600],
    paddingBottom: theme.spacing(1),
  },
  BoxFullWidthImage: {
    paddingTop: '50%',
    position: 'relative',
    overflow: 'hidden',
  },
  ImageFullWidth: {
    maxWidth: '100%',
    width: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
  },
  BoxHalfWidthImage: {
    paddingTop: '100%',
    position: 'relative',
    overflow: 'hidden',
  },
  ImageHalfWidth: {
    height: '100%',
    transform: 'translate(-50%, -50%)',
    position: 'absolute',
    top: '50%',
    left: '50%',
  },
  CardActions: {
    padding: theme.spacing(2),
  },
  CardPrice: {},
})

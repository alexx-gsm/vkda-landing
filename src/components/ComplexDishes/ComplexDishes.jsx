import React, { Fragment, useState } from 'react'
import useMediaQuery from '@material-ui/core/useMediaQuery'
// components
import LinearIndeterminate from '../../components/LinearIndeterminate'
import ComplexDishDialog from '../ComplexDishDialog'
// @material-ui components
import { Typography, Container, Grid, Paper, Box } from '@material-ui/core'
import { Zoom, Fab, CardActionArea } from '@material-ui/core'
import { Tabs, Tab } from '@material-ui/core'
import AddCartIcon from '@material-ui/icons/AddShoppingCart'
// utils
import moment from 'moment'
import 'moment/locale/ru'

function ComplexDishes({
  dishes,
  complexDinners,
  loading,
  formatter,
  classes,
}) {
  const [value, setValue] = useState(0)
  const [isDialog, showDialog] = useState(false)
  const [complexDinner, setComplexDinner] = useState({})

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  const handleDialog = (dinner) => {
    setComplexDinner({
      ...dinner,
      dishes: dinner.dishes.map((id) => dishes[id]),
    })
    showDialog(true)
  }

  const isMobile = useMediaQuery('(max-width:600px)')

  return (
    <Container maxWidth="lg" className={classes.Container}>
      <Typography
        variant="h2"
        className={classes.Title}
        align="center"
        color="primary"
      >
        Комплексные обеды
      </Typography>

      <Tabs
        value={value}
        onChange={handleChange}
        indicatorColor="secondary"
        textColor="primary"
        centered
        className={classes.TabsWrap}
      >
        {[...Array(5)].map((day, i) => (
          <Tab
            key={i}
            color="primary"
            label={moment()
              .day(i + 1)
              .format(isMobile ? 'dd' : 'dddd')}
          />
        ))}
      </Tabs>

      <Container maxWidth="md">
        {[...Array(5)].map(
          (day, i) =>
            value === i && (
              <Grid container alignItems="center" spacing={4} key={i}>
                {['salad', 'full', 'soup'].map((alias) => {
                  const dinner = complexDinners[alias]
                  return (
                    <Grid
                      item
                      key={alias}
                      xs={12}
                      sm={12}
                      md={4}
                      className={classes.GridCard}
                    >
                      <Zoom
                        in={true}
                        style={
                          dinner.dishes[3]
                            ? { transitionDelay: 0 }
                            : { transitionDelay: '200ms' }
                        }
                      >
                        <Paper className={classes.CardWrap}>
                          <CardActionArea onClick={() => handleDialog(dinner)}>
                            <Typography
                              variant="h4"
                              align="center"
                              className={classes.CardTitle}
                              color="primary"
                            >
                              "{dinner.title}"
                            </Typography>
                            <Typography
                              variant="subtitle1"
                              align="center"
                              className={classes.CardSubTitle}
                            >
                              {dinner.desc}
                            </Typography>

                            <Box className={classes.BoxFullWidthImage}>
                              <img
                                src={`assets/images/dishes/${
                                  dishes[dinner.dishes[0]].image
                                }`}
                                alt={dishes[dinner.dishes[0]].title}
                                className={classes.ImageFullWidth}
                              />
                            </Box>

                            <Grid container direction="row" wrap="nowrap">
                              <Grid item xs={6}>
                                <Box className={classes.BoxHalfWidthImage}>
                                  <img
                                    src={`assets/images/dishes/${
                                      dishes[dinner.dishes[1]].image
                                    }`}
                                    alt={dishes[dinner.dishes[1]].title}
                                    className={classes.ImageHalfWidth}
                                  />
                                </Box>
                              </Grid>
                              <Grid item xs={6}>
                                <Box className={classes.BoxHalfWidthImage}>
                                  <img
                                    src={`assets/images/dishes/${
                                      dishes[dinner.dishes[2]].image
                                    }`}
                                    alt={dishes[dinner.dishes[2]].title}
                                    className={classes.ImageHalfWidth}
                                  />
                                </Box>
                              </Grid>
                            </Grid>
                            {dinner.dishes[3] && (
                              <Box className={classes.BoxFullWidthImage}>
                                <img
                                  src={`assets/images/dishes/${
                                    dishes[dinner.dishes[3]].image
                                  }`}
                                  alt={dishes[dinner.dishes[3]].title}
                                  className={classes.ImageFullWidth}
                                />
                              </Box>
                            )}
                          </CardActionArea>
                          <Grid
                            container
                            justify="space-between"
                            alignItems="center"
                            className={classes.CardActions}
                          >
                            <Grid item>
                              <Typography
                                variant="h5"
                                color="secondary"
                                className={classes.CardPrice}
                              >
                                {formatter.format(dinner.price)}
                              </Typography>
                            </Grid>
                            <Grid item>
                              <Fab
                                color="secondary"
                                aria-label="Add"
                                // onClick={handleAdd}
                                size="medium"
                              >
                                <AddCartIcon />
                              </Fab>
                            </Grid>
                          </Grid>
                        </Paper>
                      </Zoom>
                    </Grid>
                  )
                })}
              </Grid>
            )
        )}
      </Container>

      {isDialog && (
        <ComplexDishDialog
          open={isDialog}
          handleClose={() => showDialog(false)}
          complexDinner={complexDinner}
        />
      )}

      {!loading ? <Fragment /> : <LinearIndeterminate />}
    </Container>
  )
}

export default ComplexDishes

export const dishes = {
  '1': {
    _id: '1',
    title: 'Солянка',
    content: 'вода, картоха, томаты, мясо',
    price: '50',
    weight: '300',
    category: '',
    image: '1.jpeg',
  },
  '7': {
    _id: '7',
    title: 'Оригинальный',
    content: 'капуста, морковь, колбаса, майонез',
    price: '45',
    weight: '300',
    category: '',
    image: '4.jpeg',
  },
  '3': {
    _id: '3',
    title: 'Котлета «Особая»',
    content: 'свинина, говядина, лук, панировочные сухари',
    price: '73',
    weight: '100',
    category: '',
    image: 'meat.jpg',
  },
  '4': {
    _id: '4',
    title: 'Жареный картофель',
    content: 'жареный картофель, лук',
    price: '30',
    weight: '200',
    category: '',
    image: 'garnir.jpg',
  },
}

export const complexDinners = {
  salad: {
    title: 'Салатный',
    desc: 'Салат + Горячее',
    price: '135',
    dishes: ['7', '3', '4'],
  },
  soup: {
    title: 'Супный',
    desc: 'Суп + Горячее',
    price: '195',
    dishes: ['1', '3', '4'],
  },
  full: {
    title: 'Сытный',
    desc: 'Суп + Горячее + Салат',
    price: '165',
    dishes: ['1', '3', '4', '7'],
  },
}

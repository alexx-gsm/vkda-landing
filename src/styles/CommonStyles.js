export default (theme) => ({
  _MTAuto: {
    marginTop: 'auto',
  },
  _MBAuto: {
    marginBottom: 'auto',
  },
  _MRAuto: {
    marginRight: 'auto',
  },
  _MLAuto: {
    marginLeft: 'auto',
  },
  _MB4: {
    marginBottom: theme.spacing(4),
  },
  _MB8: {
    marginBottom: theme.spacing(8),
  },
  _PT5: {
    paddingTop: theme.spacing(5),
  },
})

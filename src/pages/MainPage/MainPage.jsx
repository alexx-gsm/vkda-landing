import React, { Fragment } from 'react'
// components
import LinearIndeterminate from '../../components/LinearIndeterminate'
import TopPanel from '../../components/TopPanel'
import Slider from '../../components/Slider'
import ComplexDishes from '../../components/ComplexDishes'
import FavoriteDish from '../../components/FavoriteDish'
import Discounts from '../../components/Discounts'
import FeedBack from '../../components/FeedBack'
import Footer from '../../components/Footer'
// @material-ui components
import { Typography } from '@material-ui/core'
// @material-ui styles
import { MuiThemeProvider } from '@material-ui/core/styles'

function MainPage({ theme, loading, classes }) {
  return (
    <MuiThemeProvider theme={theme}>
      {!loading ? (
        <Fragment>
          <TopPanel />

          <Slider />

          <ComplexDishes />

          <FavoriteDish />

          <Discounts />

          <FeedBack />

          <Footer />
        </Fragment>
      ) : (
        <Fragment>
          <Typography variant="h6" className={classes._PageTitle}>
            загрузка...
          </Typography>
          <LinearIndeterminate />
        </Fragment>
      )}
    </MuiThemeProvider>
  )
}

export default MainPage

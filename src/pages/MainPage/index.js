// styles
import { withStyles } from '@material-ui/styles'
import styles from './styles'
// component
import MainPage from './MainPage'

export default withStyles(styles)(MainPage)

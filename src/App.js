import React, { lazy, Suspense } from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
// components
import LinearIndeterminate from './components/LinearIndeterminate'
// theme
import theme from './styles/theme'

const MainPage = lazy(() => import('./pages/MainPage'))
const MenuPage = lazy(() => import('./pages/MenuPage'))

function App() {
  return (
    <Router>
      <Suspense fallback={<LinearIndeterminate />}>
        <Route path='/' exact render={() => <MainPage theme={theme} />} />
        <Route path='/menu' render={() => <MenuPage theme={theme} />} />
      </Suspense>
    </Router>
  )
}

export default App
